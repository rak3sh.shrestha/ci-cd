const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("<h1>Welcome to the jungle</h1>");
});

app.get("/api/drivers", (req, res) => {
  res.json({
    message: "Drivers api",
    data: {
      drivers: [
        { name: "system drivers" },
        { name: "network drivers" },
        { name: "adminstratives drivers" },
      ],
    },
  });
});

app.get("/api/user", (req, res) => {
  res.json({
    message: "User",
    data: {
      name: "Rakesh Shrestha",
      age: 21,
      position: "junior developer",
    },
  });
});

app.get("/api/accounts/:id", (req, res) => {
  const { id } = req.params;
  res.json({
    message: "User account",
    data: {
      id,
      name: "Rakesh Shrestha",
      age: 21,
      position: "junior developer",
    },
  });
});

app.listen(3000, () => {
  console.log("app started at port 4000");
});
